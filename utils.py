import pickle
import numpy as np

def save_obj(obj, name ):
    with open('./data/'+ name + '.pkl', 'wb') as f:
        pickle.dump(obj, f)
def load_obj(name ):
    with open('./data/' + name + '.pkl', 'rb') as f:
        return pickle.load(f)
def shuffle_unision(a, b):
    rng_state = np.random.get_state()
    ra=np.random.shuffle(a)
    np.random.set_state(rng_state)
    rb=np.random.shuffle(b)