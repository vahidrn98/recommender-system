#!/usr/bin/env python
# coding: utf-8

# In[14]:


import numpy as np
import nltk
import pandas as pd
import matplotlib.pyplot as plt
from sklearn.model_selection import train_test_split
from time import perf_counter
import gensim
import math
import pprint
from collections import defaultdict
from tensorflow import keras
from numba import jit
from funk_svd import SVD


# In[15]:


movies = pd.read_csv('data/movies.csv')
ratings = pd.read_csv('data/ratings.csv')
tags = pd.read_csv('data/tags.csv')
genres = pd.read_csv('data/genres.csv')


# In[16]:


data = pd.merge(left=movies, right=ratings, on='movieId')
data.drop_duplicates(inplace=True)
tags.drop_duplicates(inplace=True)


# In[17]:


print(data.head())


# In[18]:


data.isnull().sum()
tags.dropna(inplace=True)
tags.isnull().sum()


# In[19]:


data.isna().sum()
tags.isna().sum()


# In[20]:


print("# of movies: "+str(data["movieId"].nunique()))


# In[21]:


print("# of users: "+str(data["userId"].nunique()))


# In[23]:


years = []

for title in data['title']:
    year_subset = title[-5:-1]
    try: years.append(int(year_subset))
    except: years.append(9999)

data['moviePubYear'] = years
print(len(data[data['moviePubYear'] == 9999]))


# In[24]:


def make_histogram(dataset, attribute, bins=25, bar_color='#3498db', edge_color='#2980b9', title='Title', xlab='X', ylab='Y', sort_index=False):
    if attribute == 'moviePubYear':
        dataset = dataset[dataset['moviePubYear'] != 9999]
        
    fig, ax = plt.subplots(figsize=(14, 7))
    ax.spines['top'].set_visible(False)
    ax.spines['right'].set_visible(False)
    ax.set_title(title, fontsize=24, pad=20)
    ax.set_xlabel(xlab, fontsize=16, labelpad=20)
    ax.set_ylabel(ylab, fontsize=16, labelpad=20)
    
    plt.hist(dataset[attribute], bins=bins, color=bar_color, ec=edge_color, linewidth=2)
    
    plt.xticks(rotation=45)
    
    
make_histogram(data, 'moviePubYear', title='Movies Published per Year', xlab='Year', ylab='Counts')


# In[25]:


tags.head()


# In[57]:


def create_bin_rate(num):
    N = ratings['rating'].nunique()
    width = (max(ratings['rating'])-min(ratings['rating']))/(1.322*math.log(N))
    return (math.floor(num/width))
def create_bin_year(num):
    years = [year for year in data['moviePubYear'] if year!=9999]
    N = len(np.unique(years))
   
    width = (max(years)-min(years))/(1.322*math.log(N))
    return (math.floor(num/width))


# In[58]:


# create_bin_rate(3)

create_bin_year(2000)


# In[27]:


xs = data
ys = data['rating']
x_train,x_test,y_train,y_test = train_test_split(xs,ys,test_size=0.005,shuffle=True)


# In[28]:


x_train,x_cv,y_train,y_cv = train_test_split(x_train,y_train,test_size=0.005,shuffle=True)


# In[29]:


x_train = x_train.sample(n=100000)


# In[30]:


doc_list=movies['title'].to_numpy()
genres_doc_list=movies['genres'].to_numpy()
tags_doc_list=tags['tag'].to_numpy()
len(doc_list)
print(len(doc_list))
print(len(tags_doc_list))


# In[31]:


start = perf_counter()
stoplist = nltk.corpus.stopwords.words('english')
doc_tokenized = [gensim.utils.simple_preprocess(doc) for doc in doc_list]
genres_doc_tokenized = [gensim.utils.simple_preprocess(doc) for doc in genres_doc_list]
tags_doc_tokenized = [gensim.utils.simple_preprocess(doc) for doc in tags_doc_list]
end = perf_counter()
print(str(end-start))


# In[32]:



frequency = defaultdict(int)
doc_tokenized = [[word for word in doc if word not in stoplist]for doc in doc_tokenized]
dictionary = gensim.corpora.Dictionary(doc_tokenized)
genres_doc_tokenized = [[word for word in doc if word not in stoplist]for doc in genres_doc_tokenized]
genres_dictionary = gensim.corpora.Dictionary(genres_doc_tokenized)
tags_doc_tokenized = [[word for word in doc if word not in stoplist]for doc in tags_doc_tokenized]
tags_dictionary = gensim.corpora.Dictionary(tags_doc_tokenized)
# doc_tokenized = [doc for doc in doc_tokenized if len(doc)!=0]
print(tags_doc_tokenized[:5])


# In[33]:


start = perf_counter()
BoW_corpus = [dictionary.doc2bow(doc, allow_update=True) for doc in doc_tokenized]
genres_BoW_corpus = [genres_dictionary.doc2bow(doc, allow_update=True) for doc in genres_doc_tokenized]
tags_BoW_corpus = [tags_dictionary.doc2bow(doc, allow_update=True) for doc in tags_doc_tokenized]
end = perf_counter()
print(str(end-start))


# In[21]:


pprint.pprint(BoW_corpus[0:5])


# In[34]:


gensim.corpora.MmCorpus.serialize('./data/BoW_corpus.mm', BoW_corpus)
gensim.corpora.MmCorpus.serialize('./data/genres_BoW_corpus.mm', genres_BoW_corpus)
gensim.corpora.MmCorpus.serialize('./data/tags_BoW_corpus.mm', tags_BoW_corpus)


# In[35]:


start = perf_counter()
tfidf = gensim.models.TfidfModel(BoW_corpus)
genres_tfidf = gensim.models.TfidfModel(genres_BoW_corpus)
tags_tfidf = gensim.models.TfidfModel(tags_BoW_corpus)
end = perf_counter()
print(str(end-start))
# print(BoW_corpus)


# In[36]:


corpus_tfidf = tfidf[BoW_corpus]
genres_corpus_tfidf = genres_tfidf[genres_BoW_corpus]
tags_corpus_tfidf = genres_tfidf[tags_BoW_corpus]
print(len(corpus_tfidf))
for doc in corpus_tfidf[0:10]:
    print(doc)


# In[37]:


# id2word = gensim.corpora.Dictionary(BoW_corpus)
start = perf_counter()
lsi_model = gensim.models.lsimodel.LsiModel(
   corpus=corpus_tfidf
)
genres_lsi_model = gensim.models.lsimodel.LsiModel(
   corpus=genres_corpus_tfidf
)
tags_lsi_model = gensim.models.lsimodel.LsiModel(
   corpus=tags_corpus_tfidf
)
end = perf_counter()
print(str(end-start))


# In[38]:


# print(len(genres_lsi_model.print_topics()))
# print(lsi_model.print_topics())
doc_lsi = lsi_model[corpus_tfidf]
genres_doc_lsi=genres_lsi_model[genres_corpus_tfidf]
tags_doc_lsi=genres_lsi_model[tags_corpus_tfidf]


# In[39]:


lsi_model.save('./data/LSAModel.mm')
genres_lsi_model.save('./data/genres_LSAModel.mm')
tags_lsi_model.save('./data/tags_LSAModel.mm')


# In[40]:


print(x_train['userId'].nunique()*x_train['movieId'].nunique())
mat_df = x_train[['userId','movieId','rating']]


# In[53]:


def make_pivot(mydf):
    mat = mydf.pivot_table(values="rating",index='userId',columns='movieId')


# In[54]:


make_pivot(mat_df)


# In[47]:


mat.to_csv('./data/matrix.csv')


# In[ ]:




