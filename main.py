import numpy as np
import nltk
import pandas as pd
import matplotlib.pyplot as plt
from sklearn.model_selection import train_test_split
from time import perf_counter
import gensim
import math
import pprint
from collections import defaultdict
from numba import jit
import warnings
import pickle
import torch
import utils
import torch.nn as nn
import torch.nn.functional as F
from MatrixFactorization import Factorizer
import MatrixFactorization
import matplotlib.pyplot as plt
from torchvision import transforms
from sklearn.preprocessing import normalize
from sklearn.preprocessing import MinMaxScaler
from bayes_opt import BayesianOptimization,UtilityFunction
from scipy.sparse import csr_matrix
from pandas.api.types import CategoricalDtype
from DNN import DNN,create_df,train_test,optimize_hp


def main():

    

    # In[4]:

    Net = DNN(100,0.2,0.2,0)
    Net.years=Net.zero_pad(10)
    # Net.mat_df = pd.io.parsers.read_csv('./data/matrix_small.csv',index_col=[0]).fillna(0)

    # In[39]:
    Net.mat_df = pd.io.parsers.read_csv('./data/matrix_dense.csv',index_col=[0]).fillna(0)

    opt = torch.optim.Adam(Net.parameters(),lr=0.003)

    train_loss,test_loss=train_test(Net,None,opt,300,50,50,False,0,0,0)

    optimize_hp(3)

if __name__ == "__main__":
    main()
