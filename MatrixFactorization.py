#!/usr/bin/env python
# coding: utf-8

# In[5]:


import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from time import perf_counter
import math
import pprint
# from tensorflow import keras
from numba import jit,njit
from bayes_opt import BayesianOptimization,UtilityFunction
# from funk_svd import SVD
from sklearn.metrics import precision_score
from sklearn.metrics import recall_score
from sklearn.metrics import f1_score
from sklearn.metrics import pairwise_distances


# In[6]:


class Factorizer:
    def __init__(self,maxIter,rank,alpha,lam,cold=True,dense=False):
        self.rank = rank # number of latent factors in MF
        self.alpha = alpha
        self.lam = lam    #lambda (regularization parameter)
        self.maxIter=maxIter
        start = perf_counter()
        if(dense):
            self.mat_df = pd.io.parsers.read_csv('./data/matrix_dense.csv',index_col=[0]).fillna(0)
        else:
            self.mat_df = pd.io.parsers.read_csv('./data/matrix_small.csv',index_col=[0]).fillna(0)
        end = perf_counter()
        print("loaded matrix in %d seconds",end-start)
        self.mat = self.mat_df.as_matrix()
        # demeanifying the matrix tolower the effect of cold start
        mean = np.mean(self.mat,axis=1)
        self.mat_demeaned =self.mat - mean.reshape(-1, 1)
        np.save('./data/demeaned',self.mat_demeaned)
        # releasing memory of unused variables
        self.mat =None
#         del self.mat_df
        del self.mat
        
    def train(self):
    
        return model
    
    def recommend(self,userId):
        
        return recoms
    
    def tune(self,rank,alpha,lam):
        
        return rank,alpha,lam
    


# jit annotation runs C and fortran under the hood (only for numpy and Vanilla python codes)
@jit(nopython=True)
def ALS_full(ratings, latent_factors, reg_term , iterations):
    M,N = ratings.shape
    K = int(latent_factors)
    # U: user embeddings & V: item embeddings
    U = np.random.randn(M, K)
    V = np.random.randn(K, N)
    # B and C: regularization terms
    B = np.random.randn(M,1)
    C = np.random.randn(1,N)
    errors = []
    for i in range(iterations):
        # solve: Solve a linear matrix equation, or system of linear scalar equations.
        U = np.linalg.solve(np.dot(V, V.T) + reg_term * np.eye(K), np.dot(V , ratings.T - B.T - C.T)).T
        V = np.linalg.solve(np.dot(U.T, U) + reg_term * np.eye(K), np.dot(U.T, ratings - B - C))
        B = (ratings - np.dot(U,V) - C)/(1+reg_term)
        C = (ratings - np.dot(U,V) - B)/(1+reg_term)
        # calculating RMSE
        error = np.sqrt(np.sum((ratings - (np.dot(U, V) + B + C))**2))
        print("Iteration - ",i+1," Error - ",error) 
        errors.append(error)
        if(error<1e-5):
            break
    predictor = np.dot(U,V) + B + C
    return predictor, errors,U,V



# factorizer.lam = 0.0
# factorizer.rank = 10


# optimization loop (over rank and lambda)
def optimize():
    # next params to try suggested by the algorithm
    next_point_to_probe = optimizer.suggest(utility)
    for _ in range(10):
        next_point = optimizer.suggest(utility)
        print(next_point['latent_factors'],next_point['reg_term'])
        pred,target,U,V = ALS_full(factorizer.mat_demeaned,next_point['latent_factors'],next_point['reg_term'],20)
        # the target is the list of errors but we should pass only the last one to the optimizer
        # we use the inverse because the optmizer maximizes the target but we want the error to be minimized
        target= 1/target[len(target)-1]
        optimizer.register(params=next_point, target=target)
        print(target, next_point)
    print(optimizer.max)


# In[26]:


# on first try found reg_term=0 and rank = 35 would be a very good option(with RMSE almost zero !)

# In[12]:


# pairwise cosine similarity (memory intensive but fast so we should use the single mode online)
# distance of all items from eachother
@jit(nopython=True)
def cosine_sim(xs):
    maximum = 0
    sim = 0
    shapex,shapey = xs.shape
    sims = np.zeros(shape=(shapex, shapex))
    i=0
    j=0
    for x in xs:
        j=0
        norm =np.sqrt(np.dot(x,x))
        for item in xs:
            sim = np.dot(x,item)/((norm)* np.sqrt(np.dot(item,item)))
            sims[i][j]= sim
            if(sim > maximum):
                maximum = sim
            j+=1
        i+=1
    return sims,maximum


# In[13]:


# distance of one item from the rest of the array (super fact and uses no memory)
@jit(nopython=True)
def cosine_sim_single(x,xs):
    maximum = 0
    sim = 0
    shapey = xs.shape[0]
    sims = {}
    i=0
    j=0
    norm =np.sqrt(np.dot(x,x))
    for item in xs:
        div = (norm)* np.sqrt(np.dot(item,item))
        if(div!=0):
            sim = np.dot(x,item)/(div)
            sims[j]= sim
            if(sim > maximum):
                maximum = sim
        else:
            sims[j]=0 
        j+=1
    return sims,maximum


# In[16]:

def get_similar_users(userIndex,U):
    dist , maximum =cosine_sim_single(U[userIndex],U)
#     print(dist[userIndex])
    keys =  list(dist.keys())
    np.random.shuffle(keys)
    dist = dict([(key, dist[key]) for key in keys])
    sorted_list = {k: v for k, v in sorted(dist.items(), key=lambda item: item[1],reverse=True)}
#     print(sorted_list[userIndex])
    similar_list=[]
    i=0
    for x in sorted_list:
        if(i>5):
            break
        elif(x!=userIndex):
            similar_list.append(x)
        i+=1
    return similar_list


# In[75]:


def get_similar_items(itemIndex,V):
    dist , maximum =cosine_sim_single(V[itemIndex],V)
    sorted_list = {k: v for k, v in sorted(dist.items(), key=lambda item: item[1],reverse=True)}
    similar_list = []
    i=0
#     print(sorted_list)
    for x in sorted_list:
#         print(x)
        if(i>5):
            break
        elif(x!=itemIndex):
            similar_list.append(x)
        i+=1
    return similar_list


# In[77]:


def recommend_embeddings(userIndex,U,V,table,rank,num_items):
    similar_users=get_similar_users(userIndex,U)
    item_embeddings = np.zeros(shape=(len(similar_users)*num_items, rank))
    j = 0
    item_ids =[]
    for user in similar_users:
        k=0
        interactions = dict(enumerate(table.iloc[user]))
        keys =  list(interactions.keys())
        np.random.shuffle(keys)
        interactions = dict([(key, interactions[key]) for key in keys])
#         print(interactions)
        sorted_interactions = {p: v for p, v in sorted(interactions.items(), key=lambda item: item[1],reverse=True)}
        
#         print(sorted_interactions)
        for i in sorted_interactions:
#             print(i)
            if(k>=num_items):
                break
            elif(int(table.columns[i]) not in item_ids):
                item_embeddings[j]=V[i]
                item_ids.append(int(table.columns[i]))
                j+=1
                k+=1
    return item_embeddings,item_ids


# In[78]:


def recommend_users_for_items(itemIndex,U,V,table,rank,num_users):
    similar_items=get_similar_items(itemIndex,V)
    user_embeddings = np.zeros(shape=(len(similar_items)*num_users, rank))
    j = 0
    user_ids =[]
    print(similar_items)
    for item in similar_items:
        
        k=0
        interactions = dict(enumerate(table.iloc[:,item]))
#         print(interactions)
        sorted_interactions = {p: v for p, v in sorted(interactions.items(), key=lambda item: item[1],reverse=True)}
        
#         print(sorted_interactions)
        for i in sorted_interactions:
#             print(i)
            if(k>=num_users):
                break
            elif(int(table.iloc[i].name) not in user_ids):
                user_embeddings[j]=U[i]
#                 print(table.iloc[i].name)
                user_ids.append(int(table.iloc[i].name))
                j+=1
                k+=1
    return user_embeddings,user_ids



